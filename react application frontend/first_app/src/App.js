import React from 'react'
import Navbar from './component/navbar'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import './App.css'

function App() {
  return (
    <div>
        <Router>
          <div className="app-container">
        <Navbar />
          {/* <Route path="/home" component={<Navbar />} /> */}
        </div>
        </Router>
    </div>
  );
}

export default App;
