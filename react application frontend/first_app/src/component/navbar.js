import React from 'react'
import { Link } from 'react-router-dom'

function navbar() {
    return (
        <>
            <nav className="navbar navbar-light bg-light">
                <div className="container-fluid">
                    <Link to='/' className='navbar-logo'>
                        Bootstrap <i className="fab fa-type3" />
                    </Link>
                </div>
            </nav>
        </>
    )
}

export default navbar
