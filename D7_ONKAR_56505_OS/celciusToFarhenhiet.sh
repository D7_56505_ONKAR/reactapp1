#!/bin/bash   # Shebang Line

# Clear the terminal
clear

# Read the celcius Value of Temperature from user
echo -n "Enter degree celsius temperature: "
read celsius

# convert the Temperature into Fahrenheit
fahrenheit=`echo "scale=4; $celsius*1.8 + 32" | bc`

# Reading Result
echo "$celsius degree celsius is equal to $fahrenheit degree fahrenheit"

# exit the script
exit 