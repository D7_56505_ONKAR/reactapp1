#!/bin/bash    #Shebang Line

# clear the Terminal
clear 

# Initialization of flag value / count value
count=0

# Taking input value as 'n' from user
echo -n "enter the range : "
read n

# Reading List of prime Numbers
echo "Prime number between 1 to $n is : "
echo "1"  
echo "2"

for((i=3;i<=n;)) # Outer for loop
do
    for((j=i-1;j>=2;)) # Inner for loop
    do
        if [  `expr $i % $j` -ne 0 ]
        then
            count=1
        else
            count=0
            break
        fi
        j=`expr $j - 1` 
    done
    if [ $count -eq 1 ] 
    then
        echo $i  # reading 'i' value
    fi
i=`expr $i + 1`
done

# exit the script
exit 