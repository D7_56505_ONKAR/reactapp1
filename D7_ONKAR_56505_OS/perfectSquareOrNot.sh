#!/bin/bash    # Shebang Line

# clear the terminal
clear

# Taking Input from user
echo -n "Enter the Number : "
read n

# Initializataion
v=0
i=1
x=0

while [ $i -le $n ]
do
	x=`expr $i \* $i`

	if [ "$x" = "$n" ]
	then
		v=1
		echo "The Number is a perfect square"
	fi   # If end

	i=`expr $i + 1`   # 'i' value Increment
done

if [ $v -eq 0 ]   # If condition 
then
	echo "The Number is not a perfect square"
fi  # If end

# Exit the Script
exit 